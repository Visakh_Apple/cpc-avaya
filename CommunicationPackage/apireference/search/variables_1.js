var searchData=
[
  ['kcscontactextrafieldnativeid',['kCSContactExtraFieldNativeId',['../_c_s_contact_8h.html#a90db6922313624c916086f0c94a36d45',1,'CSContact.h']]],
  ['kdevicemodelipad1',['kDeviceModeliPAD1',['../interface_c_s_i_o_s_device_model.html#a24e0463a856a45faeb02fd26edf27a67',1,'CSIOSDeviceModel']]],
  ['kdevicemodelipad2',['kDeviceModeliPAD2',['../interface_c_s_i_o_s_device_model.html#ad1dd0b07564d194cfd25ba74dd504fd6',1,'CSIOSDeviceModel']]],
  ['kdevicemodelipad3',['kDeviceModeliPAD3',['../interface_c_s_i_o_s_device_model.html#a96164c98a977b59a033157617f343863',1,'CSIOSDeviceModel']]],
  ['kdevicemodelipad4',['kDeviceModeliPAD4',['../interface_c_s_i_o_s_device_model.html#a792417338f32e6bef39a36fd3b687667',1,'CSIOSDeviceModel']]],
  ['kdevicemodelipadair',['kDeviceModeliPADAir',['../interface_c_s_i_o_s_device_model.html#a52fad358d23fda8ff9faa00e4e20a335',1,'CSIOSDeviceModel']]],
  ['kdevicemodelipadmini',['kDeviceModeliPADMini',['../interface_c_s_i_o_s_device_model.html#afd6512b44946f556ec9ef4ef8164e96f',1,'CSIOSDeviceModel']]],
  ['kdevicemodelipadminiretina',['kDeviceModeliPADMiniRetina',['../interface_c_s_i_o_s_device_model.html#a67ace9f3e3e5756a6ad65f99983d3e48',1,'CSIOSDeviceModel']]],
  ['kdevicemodelipadnewerorunknown',['kDeviceModeliPADNewerOrUnKnown',['../interface_c_s_i_o_s_device_model.html#a0ee182c02dd3337909bcc55086e49729',1,'CSIOSDeviceModel']]],
  ['kdevicemodelipadsimulator',['kDeviceModeliPADSimulator',['../interface_c_s_i_o_s_device_model.html#a132ea92f65c77ee337ddb9edeefcf099',1,'CSIOSDeviceModel']]],
  ['kdevicemodeliphone5',['kDeviceModeliPhone5',['../interface_c_s_i_o_s_device_model.html#ac683adf473ec14fb3f4beac16042470b',1,'CSIOSDeviceModel']]],
  ['kdevicemodeliphone5c',['kDeviceModeliPhone5C',['../interface_c_s_i_o_s_device_model.html#ae2113868b057a7679b709a810461624c',1,'CSIOSDeviceModel']]],
  ['kdevicemodeliphone5s',['kDeviceModeliPhone5S',['../interface_c_s_i_o_s_device_model.html#aebf9f4d5d9fb31ed616f94adb2a93aee',1,'CSIOSDeviceModel']]],
  ['kdevicemodeliphone6',['kDeviceModeliPhone6',['../interface_c_s_i_o_s_device_model.html#a7ad0ce46c923c11807a995ceb3044886',1,'CSIOSDeviceModel']]]
];
