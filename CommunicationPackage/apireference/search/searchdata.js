var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwz",
  1: "cu",
  2: "acm",
  3: "abcdefghijlmnopqrstuvw",
  4: "ck",
  5: "cgirs",
  6: "c",
  7: "c",
  8: "_abcdefghijklmnopqrstuvwz",
  9: "cd",
  10: "ad"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "properties",
  9: "defines",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Properties",
  9: "Macros",
  10: "Pages"
};

