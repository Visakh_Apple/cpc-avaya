var searchData=
[
  ['uccpadditionalfeaturesenabled',['uccpAdditionalFeaturesEnabled',['../interface_c_s_conference_configuration.html#aa5fc11d9c2b20f361d41c885fb7163b3',1,'CSConferenceConfiguration']]],
  ['uccpenabled',['uccpEnabled',['../interface_c_s_conference_configuration.html#aa5593c8e8d3aacd74fc91aac7653d572',1,'CSConferenceConfiguration']]],
  ['uccpurl',['uccpURL',['../interface_c_s_call_creation_info.html#a28149ac47417f0e39baac26df04d716c',1,'CSCallCreationInfo::uccpURL()'],['../interface_c_s_unified_portal_meeting_info.html#af2d4d8dcdf8ea64b11f17c98462553fb',1,'CSUnifiedPortalMeetingInfo::uccpURL()']]],
  ['ucid',['ucid',['../interface_c_s_user_to_user_information.html#acd47e904048f3663e563e0fa5cb2015a',1,'CSUserToUserInformation']]],
  ['unassignaslecturercapability',['unassignAsLecturerCapability',['../interface_c_s_active_participant.html#a12e5bea12ea462cc5b1a5989bb48253b',1,'CSActiveParticipant']]],
  ['unassignaspresentercapability',['unassignAsPresenterCapability',['../interface_c_s_active_participant.html#acd8e3ee022e2550deec2946ec636f2d4',1,'CSActiveParticipant']]],
  ['unblockcallingpartynumbercapability',['unblockCallingPartyNumberCapability',['../interface_c_s_call_feature_service.html#a2b79eb75fe25b410467384df6188a8f1',1,'CSCallFeatureService']]],
  ['unblockparticipantvideocapability',['unblockParticipantVideoCapability',['../interface_c_s_active_participant.html#ac864541925d264c2c61c8ce4bb423b54',1,'CSActiveParticipant']]],
  ['unblockselfvideocapability',['unblockSelfVideoCapability',['../interface_c_s_conference.html#a6a9ddb0d50f3fdd3b36fe4554e103b73',1,'CSConference']]],
  ['unconditionalcallforwardingstatus',['unconditionalCallForwardingStatus',['../interface_c_s_enhanced_call_forwarding_status.html#a4293f2e17ac834e79ad29c258a999562',1,'CSEnhancedCallForwardingStatus']]],
  ['unholdcapability',['unholdCapability',['../interface_c_s_call.html#ad38dcf7e2adbbc6cd8c63ebd36f8b743',1,'CSCall']]],
  ['unifiedportalservice',['unifiedPortalService',['../interface_c_s_user.html#abd4521078baab707884460d7373d0c40',1,'CSUser']]],
  ['unifiedportalservicestatus',['unifiedPortalServiceStatus',['../interface_c_s_user.html#a65535210962a19f6ad318329dda402ce',1,'CSUser']]],
  ['uniqueaddressformatching',['uniqueAddressForMatching',['../interface_c_s_contact.html#a58e64d6c0b1280fcb7042f62e1074539',1,'CSContact::uniqueAddressForMatching()'],['../protocol_c_s_contact-p.html#a2cea23a2cbef603164fdcbe6676684b2',1,'CSContact-p::uniqueAddressForMatching()'],['../interface_c_s_editable_contact.html#a3be0fec0e75d11e5813152849e7849e9',1,'CSEditableContact::uniqueAddressForMatching()']]],
  ['uniqueid',['uniqueId',['../interface_c_s_team_button_incoming_call.html#a3664cbb281123be8d53598495b0d5565',1,'CSTeamButtonIncomingCall']]],
  ['unmuteallparticipantscapability',['unmuteAllParticipantsCapability',['../interface_c_s_conference.html#a3cab81b1a517300829645c2e5deec879',1,'CSConference']]],
  ['unmutecapability',['unmuteCapability',['../interface_c_s_call.html#a679d41f15f1dae31b34533283f6bba22',1,'CSCall']]],
  ['unmuteparticipantaudiocapability',['unmuteParticipantAudioCapability',['../interface_c_s_active_participant.html#a7e6de351c78f38e678cb8f237b652669',1,'CSActiveParticipant']]],
  ['unmuteselfaudiocapability',['unmuteSelfAudioCapability',['../interface_c_s_conference.html#afd430f28651f4cf86cd8da01d93c2bbb',1,'CSConference']]],
  ['unparkcallcapability',['unparkCallCapability',['../interface_c_s_call_feature_service.html#ad934d6b7694ba94890c93b00ea3782d2',1,'CSCallFeatureService']]],
  ['unreadattachmentcount',['unreadAttachmentCount',['../interface_c_s_messaging_conversation.html#a1df7f08e5d4d11361ac871b984d60f2c',1,'CSMessagingConversation']]],
  ['unreadmessagecount',['unreadMessageCount',['../interface_c_s_messaging_conversation.html#a2bf2a7399f5fd469cc29adfa48b22696',1,'CSMessagingConversation']]],
  ['unsilencespeakercapability',['unsilenceSpeakerCapability',['../interface_c_s_call.html#ad2e39ece9d243f7a13ae9ebe0d0e53ab',1,'CSCall']]],
  ['updatebodycapability',['updateBodyCapability',['../interface_c_s_message.html#a4dd0227bce51201e61102b130635acc4',1,'CSMessage']]],
  ['updatecapability',['updateCapability',['../interface_c_s_contact.html#a6ae018f58ac3201f68696452e47e6db4',1,'CSContact']]],
  ['updatecontactcapability',['updateContactCapability',['../interface_c_s_contact_service.html#a3123f98b34acfaa934592491f3c1cca6',1,'CSContactService']]],
  ['updatecontinuationstatuscapability',['updateContinuationStatusCapability',['../interface_c_s_conference.html#aa6f0a6e5d1b90051a64ba8fa84c6d579',1,'CSConference']]],
  ['updatedisplayvideoparticipantnamecapability',['updateDisplayVideoParticipantNameCapability',['../interface_c_s_conference.html#aa70a55dc114708ea4109d36224091b72',1,'CSConference']]],
  ['updatedonotforwardcapability',['updateDoNotForwardCapability',['../interface_c_s_message.html#a95034325b287f67778163549ac627098',1,'CSMessage']]],
  ['updateentryexittonestatuscapability',['updateEntryExitToneStatusCapability',['../interface_c_s_conference.html#a5466df1adc7b9b4a51fbed326b1603a0',1,'CSConference']]],
  ['updateimportancecapability',['updateImportanceCapability',['../interface_c_s_message.html#ae4d657190df0f86f3ef309ddba801a18',1,'CSMessage']]],
  ['updateinreplytocapability',['updateInReplyToCapability',['../interface_c_s_message.html#ac295c4fc79fb8b02fc1711cbf9638156',1,'CSMessage']]],
  ['updateisgeneratedcontentcapability',['updateIsGeneratedContentCapability',['../interface_c_s_messaging_attachment.html#affb93e663bfe149573a4f12ffbd3843f',1,'CSMessagingAttachment']]],
  ['updateisthumbnailcapability',['updateIsThumbnailCapability',['../interface_c_s_messaging_attachment.html#adba3606e1a1b2ff4b6e7b1474403ca47',1,'CSMessagingAttachment']]],
  ['updatelastaccessedtimecapability',['updateLastAccessedTimeCapability',['../interface_c_s_messaging_conversation.html#ad5a326e68fbe4e4fdffb998ac1b9deee',1,'CSMessagingConversation']]],
  ['updatelecturemodecapability',['updateLectureModeCapability',['../interface_c_s_conference.html#aa5806e7c62c9a3efd4dd4c68e6d1cf9c',1,'CSConference']]],
  ['updatelocationcapability',['updateLocationCapability',['../interface_c_s_messaging_attachment.html#ac6765506ede6743989791d26218aeed7',1,'CSMessagingAttachment']]],
  ['updatelockcapability',['updateLockCapability',['../interface_c_s_conference.html#a20eddd2ab5d77642f249b80f15031cee',1,'CSConference']]],
  ['updatemimetypecapability',['updateMimeTypeCapability',['../interface_c_s_messaging_attachment.html#af7d810898e4905b1d2b0d00c2083818b',1,'CSMessagingAttachment']]],
  ['updatemultiplepresenterscapability',['updateMultiplePresentersCapability',['../interface_c_s_conference.html#ae04aeb1b70100d55b3c76244febecdaa',1,'CSConference']]],
  ['updatenamecapability',['updateNameCapability',['../interface_c_s_messaging_attachment.html#a3b4bc1fd6a685157789a93069e7f536a',1,'CSMessagingAttachment']]],
  ['updaterefreshmodecapability',['updateRefreshModeCapability',['../interface_c_s_messaging_service.html#a157be6ba759a5683e252ddd47fffe707',1,'CSMessagingService']]],
  ['updatesensitivitycapability',['updateSensitivityCapability',['../interface_c_s_messaging_conversation.html#a33999de0dce9636d6f8b3ebde415b5e2',1,'CSMessagingConversation']]],
  ['updatesubjectcapability',['updateSubjectCapability',['../interface_c_s_messaging_conversation.html#acf85935e05a50d808bbf39bff22e88b8',1,'CSMessagingConversation']]],
  ['updatetypecapability',['updateTypeCapability',['../interface_c_s_messaging_conversation.html#abb59a8198d27067cefb4afd4a64b50b8',1,'CSMessagingConversation']]],
  ['updatevideoallowedcapability',['updateVideoAllowedCapability',['../interface_c_s_conference.html#a8af90e43d89d7bd4242d10f73f41130c',1,'CSConference']]],
  ['updatevideolayoutcapability',['updateVideoLayoutCapability',['../interface_c_s_conference.html#a7c49ad78478c23ffd89042c307a97803',1,'CSConference']]],
  ['updatevideomodecapability',['updateVideoModeCapability',['../interface_c_s_call.html#a397af34f38082b95f0a2304d3aaa3c47',1,'CSCall']]],
  ['updatevideoselfseecapability',['updateVideoSelfSeeCapability',['../interface_c_s_conference.html#ab1270998c5f85e0d054f4e3b437fcd25',1,'CSConference']]],
  ['uri',['URI',['../interface_c_s_controllable_endpoint.html#ae903efe5812841570e792c7a4d7e22cc',1,'CSControllableEndpoint']]],
  ['url',['url',['../interface_c_s_library_document.html#a8c31b38905a813423805e3116b070e7b',1,'CSLibraryDocument']]],
  ['usedevelopmentpushnotificationnetwork',['useDevelopmentPushNotificationNetwork',['../interface_c_s_a_m_m_configuration.html#a6ca15151f8bd3359cb15f3d19cb06a87',1,'CSAMMConfiguration::useDevelopmentPushNotificationNetwork()'],['../interface_c_s_i_p_office_configuration.html#a0ec6dcc2a16014e3444bd53d989ead3f',1,'CSIPOfficeConfiguration::useDevelopmentPushNotificationNetwork()'],['../interface_c_s_push_notification_configuration.html#a41f56c75812422e9dbeac97e7b1c9bea',1,'CSPushNotificationConfiguration::useDevelopmentPushNotificationNetwork()']]],
  ['useexactsipdomaincomparison',['useExactSIPDomainComparison',['../interface_c_s_s_i_p_client_configuration.html#ade08edf0aad9414bcb49ada9f9f25478',1,'CSSIPClientConfiguration']]],
  ['useprivatekeychainforidentitycertificate',['usePrivateKeychainForIdentityCertificate',['../interface_c_s_security_policy_configuration.html#af7c11ebbe30d19b7d153e609111196b4',1,'CSSecurityPolicyConfiguration']]],
  ['useradmindelegate',['userAdminDelegate',['../interface_c_s_user.html#a9016004c4f5cb30855c97aca72dcd350',1,'CSUser']]],
  ['useragentinstanceid',['userAgentInstanceId',['../interface_c_s_client_configuration.html#a7eaa8f82ca52df075e2365eac734b6ba',1,'CSClientConfiguration']]],
  ['useragentname',['userAgentName',['../interface_c_s_client_configuration.html#ad126c657cebfc991ad0de6f813d765d5',1,'CSClientConfiguration']]],
  ['userid',['userId',['../interface_c_s_s_i_p_user_configuration.html#a104abf1ac83f99bf81a411a8dbfd9e5f',1,'CSSIPUserConfiguration::userId()'],['../interface_c_s_user.html#a1dd2104162e1f25e5e3b28049216c76e',1,'CSUser::userId()']]],
  ['username',['username',['../interface_c_s_space_participant.html#ad23cc815e57cdab52f81ca99ff56f94e',1,'CSSpaceParticipant::username()'],['../interface_c_s_user_credential.html#adae9c8bd2c72b97d4d2f6194f9d8d69e',1,'CSUserCredential::username()']]],
  ['usernamepasswordsupported',['usernamePasswordSupported',['../interface_c_s_challenge.html#a6b8d86e405babbb93a080c6279c5ec39',1,'CSChallenge']]],
  ['usertouserinformation',['userToUserInformation',['../interface_c_s_call.html#a499b63fc1d09e74ab21419c2855cc3ae',1,'CSCall']]],
  ['usesipsuriovertls',['useSIPSURIOverTLS',['../interface_c_s_s_i_p_user_configuration.html#a517c65837c666156a2d04574560abc27',1,'CSSIPUserConfiguration']]],
  ['usespeaker',['useSpeaker',['../protocol_c_s_audio_file_player-p.html#a6c4f77cc4d2b3031a47d95f2a4002ddf',1,'CSAudioFilePlayer-p']]],
  ['usestream',['useStream',['../protocol_c_s_audio_file_player-p.html#a9670071cb395654f23830323797f78e6',1,'CSAudioFilePlayer-p']]],
  ['usevoippushnotificationchannelforincomingcalls',['useVoIPPushNotificationChannelForIncomingCalls',['../interface_c_s_push_notification_configuration.html#a3eb63fc000454ad0d484b1c3de76c4ab',1,'CSPushNotificationConfiguration']]]
];
