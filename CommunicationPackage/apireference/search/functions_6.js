var searchData=
[
  ['getactivesurface',['getActiveSurface',['../interface_c_s_whiteboard.html#a890d3e486163d56c506a5736feb6d763',1,'CSWhiteboard']]],
  ['getaddparticipantfromcallcapability_3a',['getAddParticipantFromCallCapability:',['../interface_c_s_conference.html#a2c7c5a083b07387c7a292a4abf03f247',1,'CSConference']]],
  ['getchatparticipantswithcompletionhandler_3a',['getChatParticipantsWithCompletionHandler:',['../interface_c_s_chat.html#a0012a991af11db1eb277bdf2b9365402',1,'CSChat']]],
  ['getdeliverydetailswithcompletionhandler_3a',['getDeliveryDetailsWithCompletionHandler:',['../interface_c_s_message.html#a1fdaceb695a9b18632e512d9a9f2df6a',1,'CSMessage']]],
  ['getinreplytomessagewithcompletionhandler_3a',['getInReplyToMessageWithCompletionHandler:',['../interface_c_s_message.html#a3bfb3138ff97ddc165ae0ecdd3440c22',1,'CSMessage']]],
  ['getlocalvideosink_3a',['getLocalVideoSink:',['../protocol_c_s_video_interface-p.html#adbf015dd259367f946b9766b7ca71b45',1,'CSVideoInterface-p']]],
  ['getmatchingcontactfordirectorycontact_3a',['getMatchingContactForDirectoryContact:',['../interface_c_s_contact_service.html#a2abb72d9d9b2d0790954a0e96d861ccc',1,'CSContactService']]],
  ['getparticipantlistwithcompletionhandler_3a',['getParticipantListWithCompletionHandler:',['../interface_c_s_collaboration.html#a38aa92a4542b2cc1663f20c48148a363',1,'CSCollaboration']]],
  ['getportalusertokenwithconfiguration_3acompletionhandler_3a',['getPortalUserTokenWithConfiguration:completionHandler:',['../interface_c_s_unified_portal_service.html#a0210bcdc7d18a7169947669fd1898712',1,'CSUnifiedPortalService']]],
  ['getportalusertokenwithconfiguration_3aconferenceid_3acompletionhandler_3a',['getPortalUserTokenWithConfiguration:conferenceId:completionHandler:',['../interface_c_s_unified_portal_service.html#a8a683673c22d60d8706d71f684916ea2',1,'CSUnifiedPortalService']]],
  ['getremotevideosource_3a',['getRemoteVideoSource:',['../protocol_c_s_video_interface-p.html#a097e3af318758cc4e1302cc77dceb2e0',1,'CSVideoInterface-p']]],
  ['getresourceswithconfiguration_3acompletionhandler_3a',['getResourcesWithConfiguration:completionHandler:',['../interface_c_s_unified_portal_service.html#a5015f3bd0a14773932136f01d6fc93ab',1,'CSUnifiedPortalService']]],
  ['getresourceswithconfiguration_3aconferenceid_3acompletionhandler_3a',['getResourcesWithConfiguration:conferenceId:completionHandler:',['../interface_c_s_unified_portal_service.html#a4ff146a5681787013f5074700b45489a',1,'CSUnifiedPortalService']]],
  ['gettelephonyeventpayloadtype',['getTelephonyEventPayloadType',['../protocol_c_s_audio_interface-p.html#a20fbeddebfc845db124dea42232df7d4',1,'CSAudioInterface-p']]]
];
