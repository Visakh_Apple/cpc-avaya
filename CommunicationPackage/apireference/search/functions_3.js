var searchData=
[
  ['dataretrieval_3adidfailwitherror_3a',['dataRetrieval:didFailWithError:',['../protocol_c_s_data_retrieval_delegate-p.html#a9893e9e944b28bc4fa9c635eee0a6311',1,'CSDataRetrievalDelegate-p']]],
  ['dataretrieval_3adidupdateprogress_3aretrievedcount_3atotalcount_3a',['dataRetrieval:didUpdateProgress:retrievedCount:totalCount:',['../protocol_c_s_data_retrieval_delegate-p.html#af5bfaefc33d467b8fa5359ebce7691ba',1,'CSDataRetrievalDelegate-p']]],
  ['dataretrievaldidcomplete_3a',['dataRetrievalDidComplete:',['../protocol_c_s_data_retrieval_delegate-p.html#a33da23f5f41dd468a338d0d123e208a2',1,'CSDataRetrievalDelegate-p']]],
  ['dataretrievalwatcher_3acontentsdidchange_3achangeditems_3a',['dataRetrievalWatcher:contentsDidChange:changedItems:',['../protocol_c_s_data_retrieval_watcher_delegate-p.html#a8be3464a5c41b97c9b64ed1519912853',1,'CSDataRetrievalWatcherDelegate-p']]],
  ['dataretrievalwatcher_3adidfailwitherror_3a',['dataRetrievalWatcher:didFailWithError:',['../protocol_c_s_data_retrieval_watcher_delegate-p.html#a0e71bb549f709587daa92963321e433f',1,'CSDataRetrievalWatcherDelegate-p']]],
  ['dataretrievalwatcher_3adidupdateprogress_3aretrievedcount_3atotalcount_3a',['dataRetrievalWatcher:didUpdateProgress:retrievedCount:totalCount:',['../protocol_c_s_data_retrieval_watcher_delegate-p.html#a5adda0b045a050fa276d737982dbbb8c',1,'CSDataRetrievalWatcherDelegate-p']]],
  ['dataretrievalwatcherdidcomplete_3a',['dataRetrievalWatcherDidComplete:',['../protocol_c_s_data_retrieval_watcher_delegate-p.html#a9294214a42b62b6846f1f053643d306c',1,'CSDataRetrievalWatcherDelegate-p']]],
  ['dataset_3adidchange_3avaluesatindexes_3a',['dataSet:didChange:valuesAtIndexes:',['../protocol_c_s_data_set_delegate-p.html#a734b258174556ceeee7b1ea0cdee966a',1,'CSDataSetDelegate-p']]],
  ['datasetinvalidated_3a',['dataSetInvalidated:',['../protocol_c_s_data_set_delegate-p.html#aff2e1b18139e96c41f6c3bc921d8808c',1,'CSDataSetDelegate-p']]],
  ['deactivateotherphonemodewithcompletionhandler_3a',['deactivateOtherPhoneModeWithCompletionHandler:',['../interface_c_s_other_phone_service.html#a225831bd86919e1c884e19ca8952a1ac',1,'CSOtherPhoneService']]],
  ['deactivatesharedcontrolwithcompletionhandler_3a',['deactivateSharedControlWithCompletionHandler:',['../interface_c_s_shared_control_service.html#ab802af923314c0ed40a2eb9b1d7e482c',1,'CSSharedControlService']]],
  ['deactivatewithcompletionhandler_3a',['deactivateWithCompletionHandler:',['../interface_c_s_push_notification_service.html#a3360858130e87198847e8814acfa3cc8',1,'CSPushNotificationService']]],
  ['deletecertstore',['deleteCertStore',['../interface_c_s_certificate_manager.html#a7c82a4f531dc842d22d9d04da7eb8c12',1,'CSCertificateManager']]],
  ['deleteclientidentitywitherror_3a',['deleteClientIdentityWithError:',['../interface_c_s_certificate_manager.html#a09a38cff34a673d7f7c8aef8203f1a52',1,'CSCertificateManager']]],
  ['deletecontact_3acompletionhandler_3a',['deleteContact:completionHandler:',['../interface_c_s_contact_service.html#ab7ace8ad4b39fb6d85889f73a7d2cd54',1,'CSContactService']]],
  ['deletecontactgroup_3acompletionhandler_3a',['deleteContactGroup:completionHandler:',['../interface_c_s_contact_service.html#af7cb9b04762ef9d63782c69866a6a9d3',1,'CSContactService']]],
  ['deletedocument_3a',['deleteDocument:',['../interface_c_s_library_manager.html#a4367e4eafd7b7052e712267af0877a73',1,'CSLibraryManager']]],
  ['deleteshape_3acompletionhandler_3a',['deleteShape:completionHandler:',['../interface_c_s_whiteboard_surface.html#a2ebad089057c4e501a1fbd897c00d7a7',1,'CSWhiteboardSurface']]],
  ['denyvideowithcompletionhandler_3a',['denyVideoWithCompletionHandler:',['../interface_c_s_call.html#a51a02dfa4ed9fa3e5a52c19d1efbbcb3',1,'CSCall']]],
  ['denywithcompletionhandler_3a',['denyWithCompletionHandler:',['../interface_c_s_call.html#aecd71eac6153f51651de591f982fe24c',1,'CSCall::denyWithCompletionHandler:()'],['../interface_c_s_pending_participant.html#a5462f7ab8afa6a932908c73ea8cf7c28',1,'CSPendingParticipant::denyWithCompletionHandler:()']]],
  ['denywithreason_3acompletionhandler_3a',['denyWithReason:completionHandler:',['../interface_c_s_call.html#a71da9e869425110e7402544c62ea3cdb',1,'CSCall']]],
  ['devicemodel',['deviceModel',['../interface_c_s_i_o_s_device_model.html#a438a880757d9911c76f7055708011567',1,'CSIOSDeviceModel']]],
  ['devicemodelname',['deviceModelName',['../interface_c_s_i_o_s_device_model.html#abd3ad3f13b97e50fdd37f266fb498314',1,'CSIOSDeviceModel']]],
  ['didchangeteambuttons_3a',['didChangeTeamButtons:',['../protocol_c_s_call_feature_service_delegate-p.html#a33eeb40a49f1be485a7f3257f15ece91',1,'CSCallFeatureServiceDelegate-p']]],
  ['didenrollcertificatewithsectrustref_3asecidentityref_3aresult_3a',['didEnrollCertificateWithSecTrustRef:secIdentityRef:result:',['../protocol_c_s_certificate_manager_enrollment_delegate-p.html#ac3aeed7e8bf94c0cd36fdb9d738c05dc',1,'CSCertificateManagerEnrollmentDelegate-p']]],
  ['didfinishvalidatecertificatewithsectrustref_3aerror_3a',['didFinishValidateCertificateWithSecTrustRef:error:',['../protocol_c_s_certificate_manager_delegate-p.html#aa0979fb4e068b8086c1d32ba96093451',1,'CSCertificateManagerDelegate-p']]],
  ['disablevmon',['disableVmon',['../protocol_c_s_device-p.html#a4c3bd5cc234b4e69bfc1fd7e56c190e9',1,'CSDevice-p']]],
  ['disconnectincludingroomsystem_3acompletionhandler_3a',['disconnectIncludingRoomSystem:completionHandler:',['../interface_c_s_conference_mobile_link.html#a498c76865b1925102c3bb2521a01ad51',1,'CSConferenceMobileLink']]],
  ['discoverwithwatcher_3a',['discoverWithWatcher:',['../interface_c_s_conference_mobile_link.html#a4f28016978b9ae3af79a9de43479a18e',1,'CSConferenceMobileLink']]],
  ['displaylayer_3a',['displayLayer:',['../interface_c_s_screen_sharing_listener.html#a912708ee195b306f338f358f3fc29bbe',1,'CSScreenSharingListener']]],
  ['download_3acompletionhandler_3a',['download:completionHandler:',['../interface_c_s_messaging_attachment.html#a71921d5d877f2ce3665d9ad9a48aff5a',1,'CSMessagingAttachment']]],
  ['downloadfilefromurlwithconfiguration_3asourceurl_3adestinationdirectory_3acompletionhandler_3a',['downloadFileFromUrlWithConfiguration:sourceURL:destinationDirectory:completionHandler:',['../interface_c_s_download_service.html#aada148cefa12f3c4a6fa3f791f6dc23b',1,'CSDownloadService']]],
  ['downloadfilefromurlwithconfiguration_3asourceurl_3adestinationdirectory_3aoptionalparameters_3acompletionhandler_3a',['downloadFileFromUrlWithConfiguration:sourceURL:destinationDirectory:optionalParameters:completionHandler:',['../interface_c_s_download_service.html#a63b6f7c552a5bbe8bf054acabfe0015e',1,'CSDownloadService']]],
  ['drawincontext_3a',['drawInContext:',['../interface_c_s_sharing_block.html#aeebb504e846973edaa17fef50d31aedb',1,'CSSharingBlock']]],
  ['drawsharingblock_3a',['drawSharingBlock:',['../interface_c_s_screen_sharing_listener.html#a1ec68f5a172bcfb0ac92bef802b06523',1,'CSScreenSharingListener']]],
  ['droppedparticipantdidchangedata_3a',['droppedParticipantDidChangeData:',['../protocol_c_s_dropped_participant_delegate-p.html#aabe14843b1248796a7d9570329d38799',1,'CSDroppedParticipantDelegate-p']]]
];
