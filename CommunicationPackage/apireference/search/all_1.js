var searchData=
[
  ['aadsurl',['aadsURL',['../interface_c_s_unified_portal_resources.html#af0d1cb8b5a07e84bd2c26ff0c02270f5',1,'CSUnifiedPortalResources']]],
  ['abbreviateddelayedringcycles',['abbreviatedDelayedRingCycles',['../interface_c_s_call.html#a0578d84bcd403ce1d93a11939004251d',1,'CSCall']]],
  ['accept',['accept',['../interface_c_s_call.html#ae4aacab8f50c3bc7e8ec29697ed8e1a9',1,'CSCall']]],
  ['acceptdenypendingparticipantcapability',['acceptDenyPendingParticipantCapability',['../interface_c_s_conference.html#a0ebe0688a1ca411765fcfc288ffd8787',1,'CSConference']]],
  ['acceptvideo_3acompletionhandler_3a',['acceptVideo:completionHandler:',['../interface_c_s_call.html#aa2518938b46dab44326a2599ac9f43ff',1,'CSCall']]],
  ['acceptwithcompletionhandler_3a',['acceptWithCompletionHandler:',['../interface_c_s_pending_participant.html#afdb3f24ec2f1548d6e65301d9edc79cf',1,'CSPendingParticipant']]],
  ['accesstoken',['accessToken',['../interface_c_s_user_credential.html#ad620f3e9eedbd1d1c2e36b16cbaa9883',1,'CSUserCredential']]],
  ['accesstokenurl',['accessTokenURL',['../interface_c_s_zang_configuration.html#aef73f2eed42a25a1dbafb5f0cdbcf985',1,'CSZangConfiguration']]],
  ['acdcall',['acdCall',['../interface_c_s_call.html#ac4860bb71d4080be79d5721cbd3ca267',1,'CSCall::acdCall()'],['../interface_c_s_call_log_item.html#a6d548c05ea0311d3b4d8af892f4e702c',1,'CSCallLogItem::acdCall()']]],
  ['acquirecollaborationdetailswithcompletionhandler_3a',['acquireCollaborationDetailsWithCompletionHandler:',['../interface_c_s_collaboration.html#aed534eb4f8dec094c4adbd57a9d69eeb',1,'CSCollaboration']]],
  ['acsconfiguration',['ACSConfiguration',['../interface_c_s_user_configuration.html#a88652d3c7a1c70c391c33fe801072d6b',1,'CSUserConfiguration']]],
  ['action',['action',['../interface_c_s_feature_invocation_parameters.html#a29302a9ea7f1dfc1af715fa24cd926b5',1,'CSFeatureInvocationParameters']]],
  ['activatemaliciouscalltracewithcompletionhandler_3a',['activateMaliciousCallTraceWithCompletionHandler:',['../interface_c_s_call_feature_service.html#a22c0cc2ea3f6ac051069a4cf03ed4f6a',1,'CSCallFeatureService']]],
  ['activateotherphonemodewithnumber_3acompletionhandler_3a',['activateOtherPhoneModeWithNumber:completionHandler:',['../interface_c_s_other_phone_service.html#aadbe0697be3d4b268d1b9731f5aa4bd0',1,'CSOtherPhoneService']]],
  ['activatesharedcontrolwithcontrollableendpoint_3acompletionhandler_3a',['activateSharedControlWithControllableEndpoint:completionHandler:',['../interface_c_s_shared_control_service.html#a6613626582601f6a1614fdd563cce5c4',1,'CSSharedControlService']]],
  ['activationmode',['activationMode',['../interface_c_s_push_notification_configuration.html#a390b62f32a05269dd77d639fcc4a9b11',1,'CSPushNotificationConfiguration']]],
  ['active',['active',['../interface_c_s_agent_skill.html#a8189ddcad307421dbdc82e10deb2b37c',1,'CSAgentSkill::active()'],['../interface_c_s_queue_statistics.html#ac0348883bd705bb07e4ccd41808bab57',1,'CSQueueStatistics::active()'],['../interface_c_s_messaging_conversation.html#a191af67bf790395d4f6e4381d096b7ac',1,'CSMessagingConversation::active()']]],
  ['activecall',['activeCall',['../interface_c_s_call_service.html#a987ecdee472de6a7df0a2bfa603939d0',1,'CSCallService']]],
  ['activeparticipant_3adidchangeaudiostatus_3a',['activeParticipant:didChangeAudioStatus:',['../protocol_c_s_active_participant_delegate-p.html#a694f3fc8b06ab8ba6411947d07434b15',1,'CSActiveParticipantDelegate-p']]],
  ['activeparticipant_3adidchangevideostatus_3a',['activeParticipant:didChangeVideoStatus:',['../protocol_c_s_active_participant_delegate-p.html#ace3e0e35dfd677e9c415e25dafc1e26e',1,'CSActiveParticipantDelegate-p']]],
  ['activeparticipantdidassignaslecturer_3a',['activeParticipantDidAssignAsLecturer:',['../protocol_c_s_active_participant_delegate-p.html#a0940bca615e2a322dd94a2e48357a884',1,'CSActiveParticipantDelegate-p']]],
  ['activeparticipantdidassignasmoderator_3a',['activeParticipantDidAssignAsModerator:',['../protocol_c_s_active_participant_delegate-p.html#a300a2e0dfe24301da3d66d3700f8dcf0',1,'CSActiveParticipantDelegate-p']]],
  ['activeparticipantdidassignaspresenter_3a',['activeParticipantDidAssignAsPresenter:',['../protocol_c_s_active_participant_delegate-p.html#a8b7ad71b35bd9543ef2ee359e29126cd',1,'CSActiveParticipantDelegate-p']]],
  ['activeparticipantdidchangeapplicationsharingstatus_3a',['activeParticipantDidChangeApplicationSharingStatus:',['../protocol_c_s_active_participant_delegate-p.html#aefc52b7437edae95129ee70ccbbb2f76',1,'CSActiveParticipantDelegate-p']]],
  ['activeparticipantdidchangecameraremotecontrolsupport_3a',['activeParticipantDidChangeCameraRemoteControlSupport:',['../protocol_c_s_active_participant_delegate-p.html#a34bef426d10d87de6d2d91979d3e9407',1,'CSActiveParticipantDelegate-p']]],
  ['activeparticipantdidchangeconnectionstatus_3a',['activeParticipantDidChangeConnectionStatus:',['../protocol_c_s_active_participant_delegate-p.html#ab48743efde3822d629f7cb89fb95b8b7',1,'CSActiveParticipantDelegate-p']]],
  ['activeparticipantdidlowerhand_3a',['activeParticipantDidLowerHand:',['../protocol_c_s_active_participant_delegate-p.html#ac1a49383ced7336eea5c0737b97470f0',1,'CSActiveParticipantDelegate-p']]],
  ['activeparticipantdidraisehand_3a',['activeParticipantDidRaiseHand:',['../protocol_c_s_active_participant_delegate-p.html#a2818178993ad0f6870911bce78298476',1,'CSActiveParticipantDelegate-p']]],
  ['activeparticipantdidunassignaslecturer_3a',['activeParticipantDidUnassignAsLecturer:',['../protocol_c_s_active_participant_delegate-p.html#ac0b20f9fcb71892d6004abb01c19b1fa',1,'CSActiveParticipantDelegate-p']]],
  ['activeparticipantdidunassignasmoderator_3a',['activeParticipantDidUnassignAsModerator:',['../protocol_c_s_active_participant_delegate-p.html#a7c352b7e8226be44eb69ffcbd8809332',1,'CSActiveParticipantDelegate-p']]],
  ['activeparticipantdidunassignaspresenter_3a',['activeParticipantDidUnassignAsPresenter:',['../protocol_c_s_active_participant_delegate-p.html#ab5040943c38155220dac8e65d45b1636',1,'CSActiveParticipantDelegate-p']]],
  ['activeparticipants',['activeParticipants',['../interface_c_s_conference.html#ae14c579c4b87f5a6ebc43f5e1aa567de',1,'CSConference::activeParticipants()'],['../interface_c_s_messaging_conversation.html#a8a0a49a361f18286a6d6623c1f9c2ca9',1,'CSMessagingConversation::activeParticipants()']]],
  ['activeslide',['activeSlide',['../interface_c_s_library_sharing.html#ae8193b18cc1055a60e3852cc10cfd82e',1,'CSLibrarySharing']]],
  ['activespeakervideoalwaysdisplayed',['activeSpeakerVideoAlwaysDisplayed',['../interface_c_s_conference.html#aa4b8ab7dbd68a32b3884f620c202d4af',1,'CSConference']]],
  ['activetalkercapability',['activeTalkerCapability',['../interface_c_s_conference.html#af123f86665603f179042c42acbeb9a6b',1,'CSConference']]],
  ['activetalkerparticipants',['activeTalkerParticipants',['../interface_c_s_conference.html#a9944f7b1ef6ae0121ff053f5a4fc8164',1,'CSConference']]],
  ['activetalkers',['activeTalkers',['../interface_c_s_conference.html#a4180081d5ff4d57abd70f794bc1edde8',1,'CSConference']]],
  ['activevideosourceparticipants',['activeVideoSourceParticipants',['../interface_c_s_conference.html#a96fe75f6de4b9b4061b3c9defdd327c3',1,'CSConference']]],
  ['actualbitrate',['actualBitrate',['../interface_c_s_collaboration_statistics.html#a9c01c118590ab351f2c92da8c396c486',1,'CSCollaborationStatistics::actualBitrate()'],['../interface_c_s_video_statistics.html#a281b83c06394198d59363ff9f483e35e',1,'CSVideoStatistics::actualBitrate()']]],
  ['actualframerate',['actualFrameRate',['../interface_c_s_collaboration_statistics.html#af1563fec17a16b7c328c9de6df00371a',1,'CSCollaborationStatistics::actualFrameRate()'],['../interface_c_s_video_statistics.html#a84ccf9fba74498a6ea8abf3f8f42dac7',1,'CSVideoStatistics::actualFrameRate()']]],
  ['actuallocationurl',['actualLocationURL',['../interface_c_s_download_result_info.html#ad204782efbfb5a9d523e21d400d1d6fa',1,'CSDownloadResultInfo']]],
  ['actualsizeimageaddress',['actualSizeImageAddress',['../interface_c_s_conference_slide.html#a6e8d40b27077f7418e530d759f753058',1,'CSConferenceSlide']]],
  ['addaddress_3a',['addAddress:',['../interface_c_s_presence_list_subscription.html#ab70e3793f16334bcfa46b43b16b8bc7b',1,'CSPresenceListSubscription']]],
  ['addaudiodeviceseventlistener_3a',['addAudioDevicesEventListener:',['../protocol_c_s_device_listener-p.html#a6331c3637a2572cfd95669e33ddc540a',1,'CSDeviceListener-p']]],
  ['addcalllogscapability',['addCallLogsCapability',['../interface_c_s_call_log_service.html#afc5940dca5789a19da0c672af66a37fe',1,'CSCallLogService']]],
  ['addcalllogswitharray_3awithcompletionhandler_3a',['addCallLogsWithArray:withCompletionHandler:',['../interface_c_s_call_log_service.html#a421aecc80baea8ed461ebd41013a3e08',1,'CSCallLogService']]],
  ['addcirclewithtopleft_3abottomright_3acolor_3awidth_3aisfilled_3acompletionhandler_3a',['addCircleWithTopLeft:bottomRight:color:width:isFilled:completionHandler:',['../interface_c_s_whiteboard_surface.html#a4803192fae9b801de9c6c7927e44ff36',1,'CSWhiteboardSurface']]],
  ['addcollaborationcapability',['addCollaborationCapability',['../interface_c_s_call.html#a87f912c2b7d863076e88c2222049a145',1,'CSCall']]],
  ['addcollaborationwithcompletionhandler_3a',['addCollaborationWithCompletionHandler:',['../interface_c_s_call.html#ad1df2f30ecd113c677a9d770c6d46451',1,'CSCall']]],
  ['addcontact_3acompletionhandler_3a',['addContact:completionHandler:',['../interface_c_s_contact_service.html#ad90972c59e934944d9ff8dc77fe19688',1,'CSContactService']]],
  ['addcontactcapability',['addContactCapability',['../interface_c_s_contact_service.html#a413e84f86283d6d3d92f6428edc8875c',1,'CSContactService']]],
  ['addcontacts_3acompletionhandler_3a',['addContacts:completionHandler:',['../interface_c_s_contact_group.html#a85a2a79c60dabf7a2c84a332fea3d689',1,'CSContactGroup']]],
  ['addcontactscapability',['addContactsCapability',['../interface_c_s_contact_group.html#a744ef80d3dd8b2f516bfa98566231637',1,'CSContactGroup']]],
  ['adddelegate_3a',['addDelegate:',['../interface_c_s_data_retrieval.html#a303674ca689a42004a102f60c5d2e71a',1,'CSDataRetrieval::addDelegate:()'],['../interface_c_s_data_retrieval_watcher.html#a46beba13df037d324f24c942ace2aa7a',1,'CSDataRetrievalWatcher::addDelegate:()'],['../interface_c_s_data_set.html#a071d54bc851cd4b9044e73ef21088507',1,'CSDataSet::addDelegate:()']]],
  ['addedby',['addedBy',['../interface_c_s_whiteboard_surface.html#a2f87507b74fd6df93867edb3fedf7494',1,'CSWhiteboardSurface']]],
  ['addedparticipants',['addedParticipants',['../interface_c_s_message.html#af71405bf4824117a3baa7e7d57833658',1,'CSMessage']]],
  ['additem_3a',['addItem:',['../interface_c_s_editable_contact_i_m_address_field_list.html#ac3089d4bfdbbb46ede1cefd2c848f28b',1,'CSEditableContactIMAddressFieldList::addItem:()'],['../interface_c_s_editable_contact_phone_field_list.html#a86f9a964d4861256a593984e2b354e7e',1,'CSEditableContactPhoneFieldList::addItem:()'],['../interface_c_s_editable_contact_string_field_list.html#a486b75265b28afeffe9fdcddae68a06c',1,'CSEditableContactStringFieldList::addItem:()'],['../interface_c_s_editable_contact_email_field_list.html#ae9cc7084e61cb1d7e668ed0719980d12',1,'CSEditableContactEmailFieldList::addItem:()']]],
  ['addminutemessage_3aoftype_3apersonal_3acompletionhandler_3a',['addMinuteMessage:ofType:personal:completionHandler:',['../interface_c_s_meeting_minutes.html#aaa26e4eb24ced05b196a7331ae5f0d67',1,'CSMeetingMinutes']]],
  ['addparticipant_3acompletionhandler_3a',['addParticipant:completionHandler:',['../interface_c_s_conference.html#af52f052b412a780dc96d891ab7783a40',1,'CSConference']]],
  ['addparticipantaddresses_3acompletionhandler_3a',['addParticipantAddresses:completionHandler:',['../interface_c_s_messaging_conversation.html#a14a952e53db17e3354fb8f0c06388c87',1,'CSMessagingConversation']]],
  ['addparticipantfromcall_3acompletionhandler_3a',['addParticipantFromCall:completionHandler:',['../interface_c_s_conference.html#ae7d38481cc6fcfaa8bb3810cd5c01d2c',1,'CSConference']]],
  ['addparticipants_3acompletionhandler_3a',['addParticipants:completionHandler:',['../interface_c_s_messaging_conversation.html#a27c088846d77a8184852bdb0feee4691',1,'CSMessagingConversation']]],
  ['addparticipantscapability',['addParticipantsCapability',['../interface_c_s_messaging_conversation.html#a51a15030a704bb2bc09ff09f43696e5b',1,'CSMessagingConversation']]],
  ['addparticipantviadialoutcapability',['addParticipantViaDialoutCapability',['../interface_c_s_conference.html#a8ec545a1d5ce9f7c3ce175a9535cd620',1,'CSConference']]],
  ['addparticipantwithcontact_3acompletionhandler_3a',['addParticipantWithContact:completionHandler:',['../interface_c_s_messaging_conversation.html#a81fc3aef17461ee07eb30a895a883276',1,'CSMessagingConversation']]],
  ['addremoteaddressdigit_3a',['addRemoteAddressDigit:',['../interface_c_s_call.html#a58bb2259e4d6c60044a695504b237e26',1,'CSCall']]],
  ['address',['address',['../interface_c_s_contact_email_address_field.html#a80fc2b524f8358e3ceb7a4db719bf7ac',1,'CSContactEmailAddressField::address()'],['../interface_c_s_conference_room_system_information.html#a776df12ab20dec17c63ef8b60090e66e',1,'CSConferenceRoomSystemInformation::address()'],['../interface_c_s_participant.html#ad03afcb7b6850620bc9c545ca716f963',1,'CSParticipant::address()']]],
  ['addshapewithpoints_3acolor_3awidth_3aisfilled_3aisfinished_3acompletionhandler_3a',['addShapeWithPoints:color:width:isFilled:isFinished:completionHandler:',['../interface_c_s_whiteboard_surface.html#a6684206ab4fca99b7e3e7f2e436df8ea',1,'CSWhiteboardSurface']]],
  ['addskill_3aforagentid_3acompletionhandler_3a',['addSkill:forAgentId:completionHandler:',['../interface_c_s_agent_service.html#a5f76745edcf0632ccf00462ffb1efe33',1,'CSAgentService']]],
  ['addsurfacecapability',['addSurfaceCapability',['../interface_c_s_whiteboard.html#a91d4fee3ecb29455eabc2a534e6eecc7',1,'CSWhiteboard']]],
  ['addterminal_3acompletionhandler_3a',['addTerminal:completionHandler:',['../interface_c_s_conference.html#a9e278496bbbc43acf8cadaa852642389',1,'CSConference']]],
  ['addterminalipaddressviadialoutcapability',['addTerminalIPAddressViaDialoutCapability',['../interface_c_s_conference.html#ab7dac18f6bf22c974eb6cb3f65fe713c',1,'CSConference']]],
  ['addtext_3aatposition_3afontsize_3acolor_3acompletionhandler_3a',['addText:atPosition:fontSize:color:completionHandler:',['../interface_c_s_whiteboard_surface.html#a10a6b998e0beae5b0d2b594c83180bc3',1,'CSWhiteboardSurface']]],
  ['addtogroupcapability',['addToGroupCapability',['../interface_c_s_contact.html#a9208d8de0125d09ad9ba9d2bc4867571',1,'CSContact']]],
  ['agentaddress',['agentAddress',['../interface_c_s_agent_information.html#aa48a2af5ca844e1093decbf403b17f73',1,'CSAgentInformation']]],
  ['agentautologinsucceeded_3a',['agentAutoLoginSucceeded:',['../protocol_c_s_agent_state_delegate-p.html#a3d58dced0530e028ccde146afa221523',1,'CSAgentStateDelegate-p']]],
  ['agentautologoutsucceeded_3a',['agentAutoLogoutSucceeded:',['../protocol_c_s_agent_state_delegate-p.html#a0a20b59ac33aecc4f73219351efae86e',1,'CSAgentStateDelegate-p']]],
  ['agentconfiguration',['agentConfiguration',['../interface_c_s_user_configuration.html#ae2dd23888390f225a8a937f60aa6bf95',1,'CSUserConfiguration']]],
  ['agentdidchangefeaturelist_3a',['agentDidChangeFeatureList:',['../protocol_c_s_agent_service_status_delegate-p.html#a4d9fd3361c9c6bec3775cc6692cf9a7a',1,'CSAgentServiceStatusDelegate-p']]],
  ['agentdidlogin_3a',['agentDidLogin:',['../protocol_c_s_agent_state_delegate-p.html#ab0cac477e6ec0b2bb600d7cdaa12c27d',1,'CSAgentStateDelegate-p']]],
  ['agentdidlogout_3a',['agentDidLogout:',['../protocol_c_s_agent_state_delegate-p.html#a5e13c4fd96a009a1077a38b436c62667',1,'CSAgentStateDelegate-p']]],
  ['agentdidupdatecapabilities_3a',['agentDidUpdateCapabilities:',['../protocol_c_s_agent_service_status_delegate-p.html#a7cc83c4a771d88a1ca49f10450e9a921',1,'CSAgentServiceStatusDelegate-p']]],
  ['agentfeaturetype',['agentFeatureType',['../interface_c_s_agent_feature.html#a9d50c0c76a2bd116f1da3efabb06020d',1,'CSAgentFeature']]],
  ['agentinformation',['agentInformation',['../interface_c_s_agent_service.html#a41ee4e0bb83388c394e364a4e3c4394c',1,'CSAgentService']]],
  ['agentlogoutoverridetime',['agentLogoutOverrideTime',['../interface_c_s_agent_information.html#a630455bf2b9be8115251926b8e1e1fb8',1,'CSAgentInformation']]],
  ['agentlogoutpending',['agentLogoutPending',['../interface_c_s_agent_information.html#a6610aeb90e41cc23462639a9f573eb79',1,'CSAgentInformation']]],
  ['agentoverridingforcedlogout',['agentOverridingForcedLogout',['../interface_c_s_agent_information.html#afdce1adb18d9cc40d415f9bb25c41486',1,'CSAgentInformation']]],
  ['agentservice',['agentService',['../interface_c_s_user.html#ad388f91d89608eae022546f16d29e56f',1,'CSUser']]],
  ['agentservice_3adidchangeagentstate_3a',['agentService:didChangeAgentState:',['../protocol_c_s_agent_state_delegate-p.html#acccd4e5b6e403d843a4f0ac6b62c0d00',1,'CSAgentStateDelegate-p']]],
  ['agentservice_3adidchangequeuestatisticslist_3a',['agentService:didChangeQueueStatisticsList:',['../protocol_c_s_agent_service_status_delegate-p.html#a08a9462629cea415e613a06a68dbbff1',1,'CSAgentServiceStatusDelegate-p']]],
  ['agentservice_3adidenableserviceobservingtype_3aforentity_3awithbuttonlamp_3a',['agentService:didEnableServiceObservingType:forEntity:withButtonLamp:',['../protocol_c_s_agent_service_status_delegate-p.html#a4807ae67fb5843a6f142bda36210a0b5',1,'CSAgentServiceStatusDelegate-p']]],
  ['agentservice_3adidfailtoperformagentautologinwitherror_3a',['agentService:didFailToPerformAgentAutoLoginWithError:',['../protocol_c_s_agent_state_delegate-p.html#a308bb94c9e0deee5f48c1853694314b1',1,'CSAgentStateDelegate-p']]],
  ['agentservice_3adidfailtoperformagentautologoutwitherror_3a',['agentService:didFailToPerformAgentAutoLogoutWithError:',['../protocol_c_s_agent_state_delegate-p.html#a2c6a0b1f788f2735df762ea768c7d86d',1,'CSAgentStateDelegate-p']]],
  ['agentservice_3adidupdateagentinformation_3a',['agentService:didUpdateAgentInformation:',['../protocol_c_s_agent_service_status_delegate-p.html#a74ea74caa21db4808ee276a382e921d6',1,'CSAgentServiceStatusDelegate-p']]],
  ['agentservice_3adidupdateworkmode_3awithresoncode_3a',['agentService:didUpdateWorkMode:withResonCode:',['../protocol_c_s_agent_service_status_delegate-p.html#a6b7e62fe0e2b9af287c85bf321430372',1,'CSAgentServiceStatusDelegate-p']]],
  ['agentserviceavailable',['agentServiceAvailable',['../interface_c_s_agent_service.html#a203b1a38d1082fe8d00ffda7e480f921',1,'CSAgentService']]],
  ['agentservicedidbecomeavailable_3a',['agentServiceDidBecomeAvailable:',['../protocol_c_s_agent_service_status_delegate-p.html#a94c3ef9436f69c816a8abcd07f19939e',1,'CSAgentServiceStatusDelegate-p']]],
  ['agentservicedidbecomeunavailable_3a',['agentServiceDidBecomeUnavailable:',['../protocol_c_s_agent_service_status_delegate-p.html#a946ea2c091791566bff97c0e288b0a64',1,'CSAgentServiceStatusDelegate-p']]],
  ['agentservicediddisableserviceobserving_3a',['agentServiceDidDisableServiceObserving:',['../protocol_c_s_agent_service_status_delegate-p.html#a3203bfe0caebf08379ca8d445c04134f',1,'CSAgentServiceStatusDelegate-p']]],
  ['agentservicestatus',['agentServiceStatus',['../interface_c_s_user.html#abe272afd22692597cd0ee2a4f266a32e',1,'CSUser']]],
  ['agentstate',['agentState',['../interface_c_s_agent_service.html#a6fc5a79d80d4c72015b97c5cad5ce376',1,'CSAgentService']]],
  ['agentworkmode',['agentWorkMode',['../interface_c_s_agent_service.html#a6f52363e7ab4fad0613671e13949bf15',1,'CSAgentService']]],
  ['alerttype',['alertType',['../interface_c_s_call.html#a3966de04e526b790cfcec5cae1b13ec9',1,'CSCall']]],
  ['alias',['alias',['../interface_c_s_contact.html#a9b6fd2329f37aece08b509906c2bfb64',1,'CSContact::alias()'],['../protocol_c_s_contact-p.html#abda3b260b7b4e2a002d00898a7b58f34',1,'CSContact-p::alias()'],['../interface_c_s_editable_contact.html#ab8f99706145e0c51222d1c54391fcfc8',1,'CSEditableContact::alias()']]],
  ['allchatmessages',['allChatMessages',['../interface_c_s_chat.html#aea5bce6f2c1c3580dc647fbde98a78e9',1,'CSChat']]],
  ['allcontacts',['allContacts',['../interface_c_s_matched_contacts_with_match_level.html#ab5df9733aab3d32289dbfd5dcb661897',1,'CSMatchedContactsWithMatchLevel']]],
  ['allmessages',['allMessages',['../interface_c_s_chat.html#aed5a7ac8d910adf1c3fe25f0905f5a48',1,'CSChat']]],
  ['allowe164passthrough',['allowE164Passthrough',['../interface_c_s_dialing_rules_configuration.html#ad4620b5facac257376c8f9674f5dc621',1,'CSDialingRulesConfiguration']]],
  ['allowed',['allowed',['../interface_c_s_capability.html#a77350b1fcb35c79d960012245eab3b6e',1,'CSCapability']]],
  ['allowedauxiliaryworkmodereasoncode',['allowedAuxiliaryWorkModeReasonCode',['../interface_c_s_agent_service.html#aa0bb391f0cce9e5b3657bb885ffcf126',1,'CSAgentService']]],
  ['allowedfiletypes',['allowedFileTypes',['../interface_c_s_video_frame_file_source.html#ae27a1125928619f40efda43b1522b2e6',1,'CSVideoFrameFileSource']]],
  ['allowedpresencewatchers',['allowedPresenceWatchers',['../interface_c_s_presence_access_control_list.html#a4902aa02c5d8d0c689e7bb5055ae3619',1,'CSPresenceAccessControlList']]],
  ['allowedvideodirection',['allowedVideoDirection',['../interface_c_s_call.html#ad8655b46ef7e0192f67070408d620fe6',1,'CSCall']]],
  ['allowpresencewatcher_3acompletionhandler_3a',['allowPresenceWatcher:completionHandler:',['../interface_c_s_presence_access_control_list.html#a6df47caf9ed1dc21bf0da26539039c15',1,'CSPresenceAccessControlList']]],
  ['allparticipants',['allParticipants',['../interface_c_s_messaging_conversation.html#a3e7405c80165f924526e959922590f45',1,'CSMessagingConversation']]],
  ['allpossiblecontactmatches',['allPossibleContactMatches',['../interface_c_s_participant.html#a9cdff89d0e5e95d2260929adbcad2994',1,'CSParticipant']]],
  ['allsubscriptionsdisabled',['allSubscriptionsDisabled',['../interface_c_s_outbound_subscription_configuration.html#a9dd7bf51716b83eaa677aa6abea5d308',1,'CSOutboundSubscriptionConfiguration']]],
  ['alternateaddressofrecord',['alternateAddressOfRecord',['../interface_c_s_s_i_p_user_configuration.html#ac94e4cff299761aa0374e75819f04e0f',1,'CSSIPUserConfiguration']]],
  ['alternatenetwork',['alternateNetwork',['../interface_c_s_s_i_p_user_configuration.html#a98207ebc8b67a81ae3f4971738461329',1,'CSSIPUserConfiguration']]],
  ['ammconfiguration',['AMMConfiguration',['../interface_c_s_user_configuration.html#afd05e953595d5de3adf25e46e62ac847',1,'CSUserConfiguration']]],
  ['analyticsenabled',['analyticsEnabled',['../interface_c_s_client_configuration.html#a15dc857b0bfddcc53912071ed77353d6',1,'CSClientConfiguration']]],
  ['anothermoderatorname',['anotherModeratorName',['../interface_c_s_pending_participant_admission_cancellation_reason.html#a0e0b3b890c368878d6efb9b4b4283c7e',1,'CSPendingParticipantAdmissionCancellationReason']]],
  ['answered',['answered',['../interface_c_s_call.html#a66883e790fc4f0f0e25f75bdd85bb904',1,'CSCall']]],
  ['anynetworkbandwidthlimitkbps',['anyNetworkBandwidthLimitKbps',['../interface_c_s_vo_i_p_configuration_video.html#a4cf618df664aa2cd8b2f7b6148f6e9b9',1,'CSVoIPConfigurationVideo']]],
  ['api_20differences',['API Differences',['../apidiffspage.html',1,'']]],
  ['apidiffspage_2emd',['apidiffspage.md',['../apidiffspage_8md.html',1,'']]],
  ['appcastcheckinterval',['appCastCheckInterval',['../interface_c_s_unified_portal_resources.html#a02e268a06add27ebd0532b1bbd89640c',1,'CSUnifiedPortalResources']]],
  ['appcasturl',['appCastURL',['../interface_c_s_unified_portal_resources.html#ae11e28fd0a6fe4445b1829402a3fe9c9',1,'CSUnifiedPortalResources']]],
  ['appcertificatesinfo',['appCertificatesInfo',['../interface_c_s_certificate_manager.html#ae9a7b5c64730ba8370444f5ecb3c4bf8',1,'CSCertificateManager']]],
  ['applicationbundleid',['applicationBundleId',['../interface_c_s_push_notification_configuration.html#abc7e8b12c7c8c13d4fca228a6c1ead72',1,'CSPushNotificationConfiguration']]],
  ['applicationsharingactive',['applicationSharingActive',['../interface_c_s_active_participant.html#a0a1e95a210fe2d0d32d9540187c60fb5',1,'CSActiveParticipant']]],
  ['applydialingrules',['applyDialingRules',['../interface_c_s_call_creation_info.html#a8d3473f2de64de5158b9110f72aa3f7f',1,'CSCallCreationInfo']]],
  ['applydialingrulesforec500_3a',['applyDialingRulesForEC500:',['../interface_c_s_call_service.html#a8b31087ca77acbd9a492e55b526369b7',1,'CSCallService']]],
  ['applyoutsidelineaccesscodetoshortnumbers',['applyOutsideLineAccessCodeToShortNumbers',['../interface_c_s_dialing_rules_configuration.html#a9bb7a58d4929d09d2b45cdd7c38c1cfb',1,'CSDialingRulesConfiguration']]],
  ['areacode',['areaCode',['../interface_c_s_dialing_rules_configuration.html#a4eecd21bb1757e735e91ecb531815e1d',1,'CSDialingRulesConfiguration']]],
  ['array',['array',['../interface_c_s_data_set.html#aee269aa607d1903c97923465d5c6f435',1,'CSDataSet']]],
  ['asai',['asai',['../interface_c_s_user_to_user_information.html#aa70739a4500cc9c3e2c59fe208d9b45c',1,'CSUserToUserInformation']]],
  ['asciialias',['asciiAlias',['../interface_c_s_contact.html#a4a87cadbfede0fd90e63f2f821917bbc',1,'CSContact::asciiAlias()'],['../protocol_c_s_contact-p.html#a84ea0f40980aab63071b6dee37be4cb9',1,'CSContact-p::asciiAlias()'],['../interface_c_s_editable_contact.html#aa73438a27c87e52d7745274ce91e582c',1,'CSEditableContact::asciiAlias()']]],
  ['asciidisplayname',['asciiDisplayName',['../interface_c_s_contact.html#a96b425628b1c1cd43dd77440131c5c52',1,'CSContact::asciiDisplayName()'],['../protocol_c_s_contact-p.html#a5d856abead9fecac9e36b53f0903f5d4',1,'CSContact-p::asciiDisplayName()'],['../interface_c_s_editable_contact.html#aadb004cbbda477523ec64987d2383fea',1,'CSEditableContact::asciiDisplayName()']]],
  ['asciifirstname',['asciiFirstName',['../interface_c_s_contact.html#a25573d1ab100600dc8a7e08d9fedd577',1,'CSContact::asciiFirstName()'],['../protocol_c_s_contact-p.html#a77cdc2d0c13f49fbdc0235aa401cb19c',1,'CSContact-p::asciiFirstName()'],['../interface_c_s_editable_contact.html#a663673165a87ec6d0894c5b18d2768a5',1,'CSEditableContact::asciiFirstName()']]],
  ['asciilastname',['asciiLastName',['../interface_c_s_contact.html#af1f4d39fc9d330d1d0342ac17e6860c0',1,'CSContact::asciiLastName()'],['../protocol_c_s_contact-p.html#a32f0dd4eeadfc035bf27244b1c476505',1,'CSContact-p::asciiLastName()'],['../interface_c_s_editable_contact.html#af1d119e96fb4fad62d5edd02bc5fb410',1,'CSEditableContact::asciiLastName()']]],
  ['assignaslecturercapability',['assignAsLecturerCapability',['../interface_c_s_active_participant.html#adc6141f135ee609bf3640839c84f06ff',1,'CSActiveParticipant']]],
  ['assignaslecturerwithcompletionhandler_3a',['assignAsLecturerWithCompletionHandler:',['../interface_c_s_active_participant.html#a95616c678dab4998db629448c8f9b895',1,'CSActiveParticipant']]],
  ['assignasmoderatorcapability',['assignAsModeratorCapability',['../interface_c_s_active_participant.html#a0c6c71c6408201d667127dd7617ca4a8',1,'CSActiveParticipant']]],
  ['assignasmoderatorwithcompletionhandler_3a',['assignAsModeratorWithCompletionHandler:',['../interface_c_s_active_participant.html#a0908bfac58e393ff1143f4f721687fd4',1,'CSActiveParticipant']]],
  ['assignaspresentercapability',['assignAsPresenterCapability',['../interface_c_s_active_participant.html#ab2c62045eed8acf08e8e8328956345b7',1,'CSActiveParticipant']]],
  ['assignaspresenterwithcompletionhandler_3a',['assignAsPresenterWithCompletionHandler:',['../interface_c_s_active_participant.html#ad0b958ca2447ab8b9ae20412355bb2c5',1,'CSActiveParticipant']]],
  ['assignrecordingnamecapability',['assignRecordingNameCapability',['../interface_c_s_conference.html#a0e457cbad77af01f0541fd4a5a34f651',1,'CSConference']]],
  ['attachmentcount',['attachmentCount',['../interface_c_s_messaging_conversation.html#af76241814d4587e618dc21bbe5c11047',1,'CSMessagingConversation']]],
  ['attachmentid',['attachmentId',['../interface_c_s_messaging_attachment.html#a9f9ed8de0f5c7c3c5519fcfa66ee1cdc',1,'CSMessagingAttachment']]],
  ['attachments',['attachments',['../interface_c_s_message.html#ad7f98fb9bfacb5c70a40579ad071ef52',1,'CSMessage']]],
  ['audioactive',['audioActive',['../interface_c_s_active_participant.html#af5971c9409ed2304ee22c2c14b66194e',1,'CSActiveParticipant']]],
  ['audioconfiguration',['audioConfiguration',['../interface_c_s_media_configuration.html#a7880f5301cc6e934e7b73940a4724dbb',1,'CSMediaConfiguration']]],
  ['audiofiledidstartplaying_3a',['audioFileDidStartPlaying:',['../protocol_c_s_audio_file_player_listener-p.html#abfcc5cebcfb03b0da41cf93a534676da',1,'CSAudioFilePlayerListener-p']]],
  ['audiofiledidstopplaying_3a',['audioFileDidStopPlaying:',['../protocol_c_s_audio_file_player_listener-p.html#a7dcbaa2f35f83d991dae6d1bbe28073c',1,'CSAudioFilePlayerListener-p']]],
  ['audiofileplayerlistener',['audioFilePlayerListener',['../protocol_c_s_audio_file_player-p.html#a69df50b4fce48412f5462b997207828c',1,'CSAudioFilePlayer-p']]],
  ['audiointerface',['audioInterface',['../interface_c_s_media_services_instance.html#ab5d4a0b48a18232edddf7f06b5d58345',1,'CSMediaServicesInstance']]],
  ['audiomuted',['audioMuted',['../interface_c_s_call.html#a30c75a6c5a23ca9a5384487cb42e0c62',1,'CSCall::audioMuted()'],['../protocol_c_s_audio_interface-p.html#ac44cfd10db1f705ae202a5b05b560bf5',1,'CSAudioInterface-p::audioMuted()']]],
  ['audiomutedbyserver',['audioMutedByServer',['../interface_c_s_active_participant.html#a7c4da056d24b3bb4d4d10dd50951c4f6',1,'CSActiveParticipant']]],
  ['audiosessiondidbecomeactive_3a',['audioSessionDidBecomeActive:',['../interface_c_s_client.html#aadfb85d2badfe190b3d385422c06ccf0',1,'CSClient']]],
  ['audiostatus',['audioStatus',['../interface_c_s_active_participant.html#a9661e4c0ca895272e8a40c06332d62f6',1,'CSActiveParticipant']]],
  ['authenticationmethodpreferences',['authenticationMethodPreferences',['../interface_c_s_download_service_configuration.html#a56a62d2a465bf14da1aaf2974422ab00',1,'CSDownloadServiceConfiguration::authenticationMethodPreferences()'],['../interface_c_s_unified_portal_configuration.html#a1b3ed006c1f7735039c9eab5a5b17ed4',1,'CSUnifiedPortalConfiguration::authenticationMethodPreferences()']]],
  ['authorizationserver',['authorizationServer',['../interface_c_s_zang_configuration.html#a7ece02de5f9f435567d2643f0c791276',1,'CSZangConfiguration']]],
  ['authorizationtoken',['authorizationToken',['../interface_c_s_call_creation_info.html#a16c918d7ddf9e5e853304a3af4146856',1,'CSCallCreationInfo']]],
  ['authorizationurl',['authorizationURL',['../interface_c_s_zang_configuration.html#a696cd3793c86c33235e34fea04f873e9',1,'CSZangConfiguration']]],
  ['authtokensupported',['authTokenSupported',['../interface_c_s_challenge.html#a6a9d11c697180631e2fe2ba7cf3e18c7',1,'CSChallenge']]],
  ['autoanswer',['autoAnswer',['../interface_c_s_agent_information.html#a13f9feab0aec79fd13aa06f877c9e289',1,'CSAgentInformation']]],
  ['autoansweradministered',['autoAnswerAdministered',['../interface_c_s_call_service.html#a975655a77db701d11512ec1885e0ca64',1,'CSCallService']]],
  ['autoanswerenabled',['autoAnswerEnabled',['../interface_c_s_call.html#a490f6a9d2e577c6a1a824b0412a621d9',1,'CSCall']]],
  ['autoawaytimeout',['autoAwayTimeout',['../interface_c_s_presence_configuration.html#a85b3738c3d4cbaf1ff971d374d12dc4a',1,'CSPresenceConfiguration::autoAwayTimeout()'],['../interface_c_s_presence_service.html#a7c7bd90205619c454b49a294b7819276',1,'CSPresenceService::autoAwayTimeout()']]],
  ['autocallbackcapability',['autoCallbackCapability',['../interface_c_s_call_feature_service.html#aa4b6f393afaca4c1eacbe0f4b0bae5ec',1,'CSCallFeatureService']]],
  ['autocallbackenabled',['autoCallbackEnabled',['../interface_c_s_call_feature_service.html#ab9db0def65de190e2ed3240b58177385',1,'CSCallFeatureService']]],
  ['autologinonstart',['autoLoginOnStart',['../interface_c_s_agent_configuration.html#a7cecaa519668e85e8eae9c1dc9861a81',1,'CSAgentConfiguration']]],
  ['autologoutonstop',['autoLogoutOnStop',['../interface_c_s_agent_configuration.html#a0722c47ab834d2874860bdf9c258b3b1',1,'CSAgentConfiguration']]],
  ['autologoutreasoncode',['autoLogoutReasonCode',['../interface_c_s_agent_configuration.html#abc37d4ae114acd61f3368639e0910930',1,'CSAgentConfiguration::autoLogoutReasonCode()'],['../interface_c_s_agent_service.html#af4433cc537d9a95bc3ceee90349d4dcd',1,'CSAgentService::autoLogoutReasonCode()']]],
  ['automaticallyupdatelastaccesstimecapability',['automaticallyUpdateLastAccessTimeCapability',['../interface_c_s_messaging_service.html#a74d03558218335e872d08a71f8852762',1,'CSMessagingService']]],
  ['automaticallyupdatelastaccesstimecapabilityforprovidertype_3a',['automaticallyUpdateLastAccessTimeCapabilityForProviderType:',['../interface_c_s_messaging_service.html#aa0c06032789237ec4992ac6d32736a38',1,'CSMessagingService']]],
  ['automaticdscpconfigurationenabled',['automaticDscpConfigurationEnabled',['../interface_c_s_vo_i_p_configuration_audio.html#a52e0edf473180c9727102f4c8a2c0d71',1,'CSVoIPConfigurationAudio::automaticDscpConfigurationEnabled()'],['../interface_c_s_vo_i_p_configuration_video.html#ae50617bf4d3e21382b1358c2d7b23c97',1,'CSVoIPConfigurationVideo::automaticDscpConfigurationEnabled()']]],
  ['automaticmodecapability',['automaticModeCapability',['../interface_c_s_presence_service.html#aa0ad982e5882ac254523d360ef78b9ea',1,'CSPresenceService']]],
  ['automaticpresencestates',['automaticPresenceStates',['../interface_c_s_presence_service.html#a57d0593ee68def6f3c40f57780be4700',1,'CSPresenceService']]],
  ['auxilaryworkmodereasoncode',['auxilaryWorkModeReasonCode',['../interface_c_s_agent_feature.html#a571a1aee0f53a6f1b64c8fe996551849',1,'CSAgentFeature']]],
  ['auxiliaryreasoncodeenforcement',['auxiliaryReasonCodeEnforcement',['../interface_c_s_agent_information.html#a8f022c0ba2b590de77e0cbd146716709',1,'CSAgentInformation']]],
  ['auxiliaryreasonlength',['auxiliaryReasonLength',['../interface_c_s_agent_information.html#a4ed9ceec0b8db8de0b7e519bbdf8be09',1,'CSAgentInformation']]],
  ['availableagentfeatures',['availableAgentFeatures',['../interface_c_s_agent_service.html#aa9a63d07e590e51d2c5b6071089a7854',1,'CSAgentService']]],
  ['availableautodials',['availableAutodials',['../interface_c_s_call_feature_service.html#af61606d9ef3b10f481d89ff0fdd50d2e',1,'CSCallFeatureService']]],
  ['availablebusyindicators',['availableBusyIndicators',['../interface_c_s_call_feature_service.html#a951204ba5f0ecafba5f61ec571e8c362',1,'CSCallFeatureService']]],
  ['availablecontactsourcetypes',['availableContactSourceTypes',['../interface_c_s_contact_service.html#a1c9bdda3bb6f636617f138f4a87a5ebc',1,'CSContactService']]],
  ['availablefeatures',['availableFeatures',['../interface_c_s_call_feature_service.html#ac98d2de0ada79f7f1556f1a6b926a808',1,'CSCallFeatureService']]],
  ['availablehuntgroupbusypositionfeatures',['availableHuntGroupBusyPositionFeatures',['../interface_c_s_call_feature_service.html#a67a544b174fefc9ebd72ff68854cc6ce',1,'CSCallFeatureService']]],
  ['avayaclientmedia_2eh',['AvayaClientMedia.h',['../_avaya_client_media_8h.html',1,'']]],
  ['avayaclientservices_2eh',['AvayaClientServices.h',['../_avaya_client_services_8h.html',1,'']]],
  ['averagelocaljittermilliseconds',['averageLocalJitterMilliseconds',['../interface_c_s_audio_details.html#a1c826b325696563925be41c4309c2b03',1,'CSAudioDetails']]],
  ['averageremotejittermilliseconds',['averageRemoteJitterMilliseconds',['../interface_c_s_audio_details.html#ada92812b36f1a5023ca1acba226fb60d',1,'CSAudioDetails']]],
  ['avaya_20client_20services',['Avaya Client Services',['../index.html',1,'']]]
];
