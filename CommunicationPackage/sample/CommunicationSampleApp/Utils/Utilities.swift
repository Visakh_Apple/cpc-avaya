//
//  Utilities.swift
//  Takaful
//
//  Created by Visakh on 27/10/20.
//  Copyright © 2020 BTeem. All rights reserved.
//

import Foundation
import UIKit
/**
 Run on main.
 */
func onmain(_ closure:@escaping ()->()) {
    DispatchQueue.main.async(execute: closure)
}
func onBackground(work: @escaping () -> ()) {
    DispatchQueue.global(qos: .background).async {
        work()
    }
}

func onmainAfterDelay(_ delay : Int, _ closure: @escaping ()->()) {
    
    let deadline = DispatchTime.now() + .seconds(delay)
    DispatchQueue.main.asyncAfter(deadline: deadline, execute: closure)

}

func appDelegate() -> AppDelegate {
    return UIApplication.shared.delegate as! AppDelegate
}

func rgb(_ rgbValue: UInt) -> UIColor {
    return rgba(rgbValue, 1.0)
}
func rgba(_ rgbValue:UInt, _ alpha:Double) -> UIColor {
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(alpha)
    )
}

func detemineAppState()  {
    
//    (UIApplication.shared.delegate as! LinphoneAppDelegate).determineActiveSceneAsPerAccountStatus()
    
}

func openSafari(_ url : String) {
    if let url = URL(string: url) {
        UIApplication.shared.open(url)
    }
}

func getCurrentTimeZone() -> String{
    return TimeZone.current.identifier
}

//func getLocale() -> Locale {
//    // Now settings the locale to India
//    return Locale(identifier: "en-IN")
//    // To set as dynamic
//    //return Locale.current
//}

func getStoryboard(_ name : String) -> UIStoryboard {
    return UIStoryboard.init(name: name, bundle: nil)
}

func imagePlaceholder() -> UIImage {
    return UIImage(named: "image-placeholder.png")!
}

func userPlaceholder() -> UIImage {
    return UIImage(named: "user-placeholder.png")!
}

func getDocumentDirectoryPath() -> String {
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
    return paths[0]
}

struct Storyboards {
    
    static let auth  = "Authentication"
    static let home  = "Home"
    static let splash = "Splash"
   
}

struct ScreenSize {
    
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    
}

struct Placeholders {
    static let apiFailed = "Something went wrong. Please try again later."
}

struct DeviceType { //Use this to check what is the device kind you're working with
    
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_SE         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_7          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_7PLUS      = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    
}


/// Generic Utilities
class Utilities {
    
    
    /// To Get App version from Bundle
    class func getAppVersion() -> String? {
        
        return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
   }
    
    
    /// Method to generate random file name
    /// - Parameter myFileName: prefix name
    class func generateUniqueFilename (myFileName: String) -> String {
        let guid = ProcessInfo.processInfo.globallyUniqueString
        let uniqueFileName = ("\(myFileName)_\(guid)")
        return uniqueFileName
    }
    
    
}
