//
//  HomeViewController.swift
//  linphone
//
//  Created by Visakh on 06/12/20.
//

import UIKit

class HomeViewController: UIViewController {

    
    
    // ******************
    // MARK:- Outlets
    // ******************
    @IBOutlet fileprivate weak var btnEmergencyCall: UIButton!
    @IBOutlet fileprivate weak var btnDialpad: UIButton!
    @IBOutlet fileprivate weak var serviceOnDemandView: UIView!
    @IBOutlet fileprivate weak var backgroundView: UIView!
    @IBOutlet fileprivate weak var footerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureUI()
    }
    
    func configureUI() {
        serviceOnDemandView.setShadowProperties(radius: 2, color: .gray, opacity: 0.4)
//        btnEmergencyCall.makeRoundedAndShadowed()
        let primaryPattern = UIImage(named: "primary-pattern.png")?.maskWithColor(color: .white)
        let secondaryPattern = UIImage(named: "secondary-pattern.png")
        self.backgroundView.backgroundColor = UIColor.init(patternImage: primaryPattern!)
        self.footerView.backgroundColor = UIColor.init(patternImage: secondaryPattern!)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // ******************
    // MARK:- Actions/Events
    // ******************
    @objc @IBAction func callButtonTapped(_ sender: Any) {
        (UIApplication.shared.delegate as! AppDelegate).determinePhoneViewController()
    }

}
